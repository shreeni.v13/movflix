package com.mfm.MovieMS.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mfm.MovieMS.service.RatingService;

@RestController
public class RatingController {
	
	@Autowired
	RatingService ratingService;
	
	@GetMapping("/addRating")
	public void addRating(int mid,String rating_value,String review,String email,HttpServletRequest request)
	{
		 ratingService.addRating(mid,rating_value,review,email);
	}
	
	@GetMapping("/getAllRating/{mid}")
	public List<Integer> getAllRatings(@PathVariable("mid") int mid)
	{
		return ratingService.getAllRating(mid);
	}

}
