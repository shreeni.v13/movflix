package com.mfm.MovieMS.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;
import com.mfm.MovieMS.service.MovieService;

@RestController
public class MovieController {

	@Autowired
	private MovieService movieservice;
	
	@GetMapping("/getAllMovies")
	public List<Movie> getAllMovies()
	{
		return movieservice.getAllMovies();
	}
	

	@PostMapping("/addMovie")
	public Movie addMovie(@RequestBody Movie m){
	
		 return movieservice.addMovie(m);
		
	}
	
	@GetMapping("/getAllMovie")
	public List<Movie> getAllMovie()
	{
		return movieservice.getAllMovie();
	}

	@GetMapping("/getAllMoviesByMovieName/{mname}")
	public List<Movie> getAllMoviesByMovieName(@PathVariable("mname") String mname)
	{
		return movieservice.getAllMoviesByMovieName(mname);
	}
	@GetMapping("/getAllMoviesByMovieCategory/{mcategory}")
	public List<Movie> getAllMoviesByMovieCategory(@PathVariable("mcategory") String mcategory)
	{
		return movieservice.getAllMoviesByCategory(mcategory);
	}
	@GetMapping("/getAllMoviesByAgeFactor/{agefactor}")
	public List<Movie> getAllMoviesByAgeFactor(@PathVariable("agefactor") String agefactor)
	{
		return movieservice.getAllMoviesByAge(agefactor);
	}
	@GetMapping("/getAllMoviesByYear/{myear}")
	public List<Movie> getAllMovies(@PathVariable("myear") int myear)
	{
		return movieservice.getAllMoviesByYear(myear);
	}

	@GetMapping("/{mid}")
    public Movie displayMovie(@PathVariable int mid){
    
        ModelAndView mv= new ModelAndView();
        Movie movie= movieservice.getMovieDetails(mid);
        mv.setViewName("MovieDetails");
        return movie;     }
	
	@DeleteMapping("/deleteMovieById/{mid}")
	public void deleteMovie(@PathVariable int mid){
		  movieservice.deleteMovie(mid);
		
	}
}
