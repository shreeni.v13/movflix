package com.mfm.MovieMS.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Rating;
import com.mfm.MovieMS.dao.MovieRepo;
import com.mfm.MovieMS.dao.RatingRepo;

@Service
@Transactional
public class RatingService {
	
	@Autowired
	RatingRepo ratingRepo;
	
	@Autowired
	MovieRepo movieRepo;
	
	@Autowired
	EntityManager em;
	
	public void addRating(int mid, String rating_value, String review,String email)
	{
		int rating=Integer.parseInt(rating_value);
		Rating r=new Rating(review,rating);
		System.out.println(review +" "+ rating);
		System.out.println("Movie:"+mid);
		r.setMovie(movieRepo.findById(mid).orElse(null));
		Customer c=em.find(Customer.class, email);
		r.setCustomer(c);
		Rating ratingSaved = ratingRepo.save(r);
		System.out.println("Rating:"+ratingSaved.getRid());
	}
	
	public void addCustomertoRating(Customer c,int rid)
	{
		System.out.println(c.getCemail()+" "+rid);
		Rating r=ratingRepo.findById(rid).orElse(null);
		r.setCustomer(c);
		ratingRepo.save(r);
	}
	
	public List<Integer> getAllRating(int mid)
	{
		return ratingRepo.getAllRatingForMovie(mid);
	}
}
