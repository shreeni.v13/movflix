package com.mfm.MovieMS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;
import com.mfm.MovieMS.dao.MovieRepo;

@Service
public class MovieService {

	@Autowired
	private MovieRepo movieRepo;
	
	public List<Movie> getAllMovies(){	
		List<Movie> movies= movieRepo.findAll();
		return movies;
	}
	
	public Movie getMovieDetails(int mid) {		
		Movie movie=movieRepo.findById(mid).orElse(new Movie());
		return movie;
	}

 public Movie addMovie(Movie m) {
		
		Movie movie=movieRepo.save(m);
		System.out.println(movie);
		return movie;
	}


public List<Movie> getAllMovie()
{
	return movieRepo.findAll();
}

	public List<Movie> getAllMoviesByMovieName(String mname){	
		List<Movie> movies= movieRepo.findByMname(mname);
		return movies;
	}
	
	public List<Movie> getAllMoviesByCategory(String mcategory){	
		List<Movie> movies= movieRepo.findByMcategory(mcategory);
		return movies;
	}
	
	public List<Movie> getAllMoviesByYear(int year){	
		List<Movie> movies= movieRepo.findByYear(year);
		return movies;
	}
	
	public List<Movie> getAllMoviesByAge(String mage){	
		List<Movie> movies= movieRepo.findByAgefactor(mage);
		return movies;
	}

	
	public void deleteMovie(int mid) {
		
		Movie movie=movieRepo.findById(mid).get();
		movieRepo.delete(movie);
	}
	

}
