package com.mfm.MovieMS.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mfm.EntityModel.Rating;

@Repository
public interface RatingRepo extends JpaRepository<Rating,Integer> {
	
	@Query("select r.rating from Rating r where r.movie.mid=?1")
	List<Integer> getAllRatingForMovie(int mid);

}
