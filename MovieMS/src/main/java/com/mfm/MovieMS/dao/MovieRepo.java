package com.mfm.MovieMS.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mfm.EntityModel.Movie;


@Repository
public interface MovieRepo extends JpaRepository<Movie,Integer> {
	List<Movie> findByMname(String mname);
	List<Movie> findByMcategory(String mcategory);
	List<Movie> findByYear(int year);
	List<Movie> findByAgefactor(String agefactor);
	

}
