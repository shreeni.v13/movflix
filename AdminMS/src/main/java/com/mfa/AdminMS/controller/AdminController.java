package com.mfa.AdminMS.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mfa.AdminMS.proxy.CustomerProxy;
import com.mfa.AdminMS.proxy.MovieProxy;
import com.mfa.AdminMS.service.AdminService;
import com.mfm.EntityModel.Admin;
import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;

@Controller
@Transactional
public class AdminController {

	@Autowired
	private CustomerProxy customerProxy;

	@Autowired
	private MovieProxy movieProxy;

	@Autowired
	private AdminService adminService;

	@Autowired
	private LoadBalancerClient loadBalancerClient;

	String loggedInAdmin = "";

	@GetMapping("/welcome")
	public ModelAndView welcome() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("welcome");
		return mv;
	}

	@PostMapping("/cregister")
	public ModelAndView cregsiter(Customer c) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("welcome");
		Customer registeredCustomer = customerProxy.register(c);
		if (registeredCustomer != null)
			mv.addObject("message", "Registration Successful. Please login to Continue");
		else
			mv.addObject("message", "Some Error Occured. Try Again!");
		return mv;
	}

	@PostMapping("/aregister")
	public ModelAndView aregsiter(Admin a) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("welcome");
		Admin registerdAdmin = adminService.register(a);
		if (registerdAdmin != null)
			mv.addObject("message",
					"Your request to register as Admin was Suucess. Please wait until it gets approved");
		else
			mv.addObject("message", "Some Error Occured. Try Again!");
		return mv;
	}

	@PostMapping("/login")
	public ModelAndView login(@ModelAttribute("type") String type, @ModelAttribute("email") String email,
			@ModelAttribute("password") String password, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String message;
		ModelAndView mv = new ModelAndView();
		mv.setViewName("welcome");
		if (type.equalsIgnoreCase("Customer")) {
			message = customerProxy.login(email, password);
			System.out.println(message);
			mv.addObject("message", message);
			if (message.equalsIgnoreCase("Login Successful")) {
				ServiceInstance instance = loadBalancerClient.choose("Customer-MS");
				URI uri = URI.create(String.format("http://%s:%s", instance.getHost(), instance.getPort()));
				System.out.println("URI:" + uri);
				String url = uri.toString();
				System.out.println("URL:" + url);
				try {
					response.sendRedirect(url + "/movie-homepage/" + email);
				} catch (IOException e1) {
					System.out.println("Error in redirecting");
				}
			} else
				return mv;
		} else {
			System.out.println(email + "===email");
			Admin loginAdmin = adminService.getByEmail(email);
			if (loginAdmin == null) {
				mv.addObject("message", "Please Register");
				mv.setViewName("welcome");
				return mv;
			} else {
				if (loginAdmin.getAemail().equalsIgnoreCase(email) && loginAdmin.getApassword().equals(password)) {
					if (loginAdmin.getAflag() == -1) {
						mv.addObject("message", "Your request to register still not processed.");
						mv.setViewName("welcome");
						return mv;
					}

					else if (loginAdmin.getAflag() == -0) {
						mv.addObject("message", "Your request to register is been declined!!");
						mv.setViewName("welcome");
						return mv;
					}

					else {
						HttpSession session = request.getSession();

						session.setAttribute("aname", adminService.getUserName(email));
						loggedInAdmin = adminService.getUserName(email);
						mv.addObject("admin", adminService.getUserName(email));
						mv.setViewName("AdminHomePage");
						return mv;
					}
				} else {
					mv.addObject("message", "Wrong Password");
					mv.setViewName("welcome");
					return mv;
				}
			}
		}
		return mv;
	}

	@GetMapping("/welcome12")
	public ModelAndView adminhome() {
		ModelAndView mv = new ModelAndView();
		mv.addObject("admin", loggedInAdmin);
		mv.setViewName("AdminHomePage");
		return mv;
	}

	@PostMapping("/addMovie")
	public ModelAndView addmovie(Movie m) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("AdminHomePage");
		Movie addMovie = movieProxy.addMovie(m);
		System.out.println("movie name: " + addMovie.getMid());
		if (addMovie != null)
			mv.addObject("message", "Movie added Successful. ");
		else
			mv.addObject("message", "Some Error Occured. Try Again!");
		return mv;
	}

	@GetMapping("/getAllCustomers")
	public ModelAndView getAllCustomers() {
		ModelAndView mv = new ModelAndView();
		List<Customer> customers = customerProxy.getAllCustomers();
		mv.setViewName("ViewCustomers");
		mv.addObject("result", customers);
		mv.addObject("admin", loggedInAdmin);
		return mv;
	}

	@GetMapping("/getAllMovie")
	public ModelAndView getAllMovie() {
		ModelAndView mv = new ModelAndView();
		List<Movie> movies = movieProxy.getAllMovie();
		mv.setViewName("ViewMovie");
		mv.addObject("result", movies);
		mv.addObject("admin", loggedInAdmin);
		return mv;
	}

	@GetMapping("/deleteMovie/{mid}")
	public String deleteMovie(@PathVariable int mid) {
		movieProxy.deleteMovie(mid);
		return "redirect:/getAllMovie";
	}

	@GetMapping("/logout")
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession(false);
		session.invalidate();
		ModelAndView mv = new ModelAndView();
		mv.setViewName("welcome");
		return mv;
	}

}
