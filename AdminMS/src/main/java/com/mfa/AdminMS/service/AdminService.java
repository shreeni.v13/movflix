package com.mfa.AdminMS.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfa.AdminMS.dao.AdminRepo;
import com.mfm.EntityModel.Admin;

@Service
public class AdminService {
	@Autowired
	private AdminRepo adminRepo;
	
	public Admin register(Admin a)
	{
		return adminRepo.save(a);
	}
	
	public String checkLoginEmail(String email,String password)
	{
		Admin loginAdmin=adminRepo.findById(email).orElse(null);
		if(loginAdmin==null)
		{
			return "Please Register";
		}
		else
		{
			if(loginAdmin.getAemail().equalsIgnoreCase(email)&&loginAdmin.getApassword().equals(password))
			{
				if(loginAdmin.getAflag()==-1)
					return "Your request to register still not processed.";
				else if(loginAdmin.getAflag()==-0)
					return "Your request to register is been declined!!";
				else return "Login Successful";
			}
			else
				return "Wrong Password";
		}
	}
	
	public Admin getByEmail(String email)
	{
		return adminRepo.findById(email).orElse(null);
	}
	
	public String getUserName(String email)
	{
		return adminRepo.getUsername(email);
	}
	
}
