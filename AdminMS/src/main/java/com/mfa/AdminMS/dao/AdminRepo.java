package com.mfa.AdminMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mfm.EntityModel.Admin;

@Repository
public interface AdminRepo extends JpaRepository<Admin,String> {

	@Query("select c.aname from Admin c where c.aemail=?1")
	public String getUsername(String email);
}
