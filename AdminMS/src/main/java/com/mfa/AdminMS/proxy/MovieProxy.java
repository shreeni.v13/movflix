package com.mfa.AdminMS.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;

import com.mfm.EntityModel.Movie;

@FeignClient(name="Movie-MS",url="localhost:8300")
public interface MovieProxy {
	
	@PostMapping("/addMovie")
	public Movie addMovie(@RequestBody Movie m);
	
	@GetMapping("/getAllMovie")
	public List<Movie> getAllMovie();
	
	@DeleteMapping("/deleteMovieById/{mid}")
	public void deleteMovie(@PathVariable int mid);
}
