package com.mfa.AdminMS.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mfm.EntityModel.Customer;

@FeignClient(name="Customer-MS",url="localhost:8100")
public interface CustomerProxy {
	@PostMapping("/register")
	public Customer register(@RequestBody Customer c);
	
	@GetMapping("/login_customer")
	public String login(@RequestParam String email,@RequestParam String password);
	
	@GetMapping("/getAllCustomers")
	public List<Customer> getAllCustomers();
}
