<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<script src="webjars/jquery/3.4.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<meta charset="ISO-8859-1">
<title>MovieFlix</title>
</head>
<body>
  
       <nav class="navbar navbar-nav navbar-inverse navbar-trans navbar-fixed-top navbar-expand-lg" role="navigation"  style="background-color:#333;">
            <a class="navbar-brand" href="#" style="font-size:40px;">
                   MovieFlix</a>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
               <div class="navbar-nav ml-auto">
               <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Register
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cregister.html">Customer</a>
          <a class="dropdown-item" href="aregister.html">Admin</a>
      </div>
      </li>
                <a class="nav-item nav-link" href="login.html">Login</a>
                <a class="nav-item nav-link" href="#">About</a>
              </div>
            </div>
          </nav>
  
    <div class="container mt-2">
    <c:set var = "m" scope = "session" value = "${message}"/>
			         <c:if test="${m !=null}">
			         <div class="alert alert-primary">
			         	<p>${m }</p>
			         </div>
       			  </c:if>
    </div>
<link href="webjars/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
</body>

</html>