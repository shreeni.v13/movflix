<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>MovieFlix</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Font Awesome 4.3.0  -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->
        <!-- Modified CSS -->
        <link rel="stylesheet" type="text/css" href="css/productlist.css">
        <link href="css/index.css" type="text/css" rel="stylesheet">
        
        <link href="css/half-slider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    </head>
    <body>
		
        <nav class="navbar navbar-nav navbar-inverse navbar-trans navbar-fixed-top navbar-expand-lg" role="navigation">
	
            <div class="container">
                <div class="navbar-header">
                    
                   <div class="navbar-brand" style="font-size:40px;"> <a href="/welcome12">MovieFlix</a>
					</div>
				</div>
				
                <div class="navbar-collapse collapse" id="navbar-collapsible">
                    <ul class="nav navbar-nav navbar-right shop-menu">
                        
						<li class="nav-item dropdown no-arrow mx-1">
						 
						<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						
						<i class="fa fa-user fa-2x"></i>
						<span class="icon-text"> ${admin}</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
							<a class="dropdown-item" href="/logout">Logout</a>
							
						</div>
						</li>
						
						
                    </ul>
                    
                </div>
			
            </div>
        </nav>
        </br></br></br></br></br></br>

<div class="container">
        <table class="table table-striped">
            <thead>
                <th>Movie Id</th>
                <th>Movie Name</th>
                <th>Movie Description</th>
                <th>Movie Category</th>
                <th>Movie Year</th>
                <th>Age Factor</th>
                <th>Delete Option</th>
            </thead>
            <tbody>
                <c:forEach items="${result}" var="r">
                    <tr>
                        <td>${r.mid}</td>
                        <td>${r.mname}</td>
                        <td>${r.mdescription}</td>
                        <td>${r.mcategory}</td>
                        <td>${r.year}</td>
                        <td>${r.agefactor}</td>
                        <td><a href="/deleteMovie/${r.mid}" class="btn btn-danger" >Delete</a></td>
                        
                    </tr>
                </c:forEach>
            </tbody>
        </table>
</div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       
        
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <!-- Pagination JS -->
        <script src="https://raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
        <!-- Rating JS -->
        <script type="text/javascript" src="js/jquery.raty.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>
    </body>
</html>