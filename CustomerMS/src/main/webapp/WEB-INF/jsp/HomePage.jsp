
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>MovieFlix</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Font Awesome 4.3.0  -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->
        <!-- Modified CSS -->
        <link rel="stylesheet" type="text/css" href="/css/productlist.css">
        <link href="/css/index.css" type="text/css" rel="stylesheet">
        
        
   
        
        
        <link href="/css/half-slider.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/jquery.raty.css">
    </head>
    <body>
		
        <nav class="navbar navbar-nav navbar-inverse navbar-trans navbar-fixed-top navbar-expand-lg" role="navigation">
	
            <div class="container">
                <div class="navbar-header">
                    
                   <div class="navbar-brand" style="font-size:40px;"> <a href="/disp">MovieFlix</a>
					</div>
				</div>
				
                <div class="navbar-collapse collapse" id="navbar-collapsible">
                    <ul class="nav navbar-nav navbar-right shop-menu">
                        
						<li class="nav-item dropdown no-arrow mx-1">
						 
						<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						
						<i class="fa fa-user fa-2x"></i>
						<span class="icon-text"> ${cname}</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
							<a class="dropdown-item" href="/logout">Logout</a>
							
						</div>
						</li>
                    </ul>
                    
                </div>
			
            </div>
        </nav>
        
	
    
	<section id="productpage" style="">
	
    <div class="row">
		<div class="container-fluid">
			<form action="/search">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 product-list">
					<div class="row">
                        <div class="container-fluid">
                            <div class="form-group">
							    <select class="form-control" name="type">
							      <option>Movie Name</option>
							      <option>Year</option>
							      <option>Category</option>
								  <option>Age Factor</option>
							    </select>
							</div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6  product-list">
				  	<div class="row">
                        <div class="container-fluid">
							<div class="form-group">
							   <input type="text" class="form-control" name="movie_cat">
							   
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 product-list">
					<div class="row">
                        <div class="container-fluid">
                            <div class="form-group">
								<span class="input-group-btn">
							        <button class="btn btn-success" type="submit">Go!</button>
							   </span>
							</div>
						</div>
					</div>
				</div>		
			</form>
		</div>
	</div>
    <br>    
	

		
        
            <div class="row">
                <div class="container-fluid">
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-search">
                        
                        
					
                        <div class="row">
                            <div class="container-fluid">
							<c:forEach var="movie" items="${movies}">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-list">
                                    <div class="row">
                                        <div class="container-fluid">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productlist-image">
                                                <img  src="${movie.getUrl1()}" alt="" />
                                            </div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 productlist-image">
                                                <img  src="${movie.getUrl2()}" alt="" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="row">
                                                    <div class="container-fluid">
                                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 product-desc">
                                                            <div class="product-name">
                                                                <h2>
                                                                <a href="/pick/${movie.getMid()}">
																<span class="text-center">
                                                                ${movie.mname}
																</span></a>
                                                                </h2>
                                                            </div>
                                                            <div class="shop-name">
																<h3>
																<span>
																${movie.getYear()}
																</span></h3>
                                                                <h2><span>
																${movie.getMcategory()}
																</span>
																</h2>
															</div>
                                                            
                                                        </div>
														<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 product-desc">
															<div class="rating-box">
															<h3>Overall Rating</h3>
																<span class="icon-text">${movie.average}</span>
                                                                <div class="rating medal readonly-rating" data-score="${ movie.average}" alt="shop-rating"></div>
                                                            </div>
															<div class="report ">
                                                              <h3><i class="glyphicon glyphicon-flag"></i> <span class="icon-text">${movie.getAgefactor()}</span></h3>
                                                            </div>
														</div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="container-fluid">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                            <div class="add-to">
                                                                <a href="/pick/${movie.getMid()}"><h3><i class="fa fa-star"></i> <span class="icon-text">Add a Review or a Rating</span></h3></a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>    
                            </div>    
						</div>
					</div>
                </div>
            </div>
        </section>
        
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <!-- Pagination JS -->
        <script src="https://raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
        <!-- Rating JS -->
        <script type="text/javascript" src="/js/jquery.raty.js"></script>
        <script type="text/javascript" src="/js/custom.js"></script>
        
    </body>
</html>