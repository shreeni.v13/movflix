<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>MovieFlix</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Font Awesome 4.3.0  -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <!-- <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> -->
        <!-- Modified CSS -->
        <!-- Modified CSS -->
        <link rel="stylesheet" type="text/css" href="/css/productinfo.css">
        <link rel="stylesheet" href="/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="/css/settings.css" media="screen" />
        <link rel="stylesheet" href="/css/animation.css">
        <link rel="stylesheet" href="/css/owl.carousel.css">
        <link rel="stylesheet" href="/css/owl.theme.css">
        <link rel="stylesheet" href="/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="/css/jquery.raty.css">
		
    </head>
    <body>
		
        <nav class="navbar navbar-nav navbar-inverse navbar-trans navbar-fixed-top navbar-expand-lg" role="navigation">
	
            <div class="container">
                <div class="navbar-header">
                    
                   <div class="navbar-brand" style="font-size:40px;"> <a href="/disp">MovieFlix</a>
					</div>
				</div>
				
                <div class="navbar-collapse collapse" id="navbar-collapsible">
                    <ul class="nav navbar-nav navbar-right shop-menu">
                        
						<li class="nav-item dropdown no-arrow mx-1">
						 
						<a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						
						<i class="fa fa-user fa-2x"></i>
						<span class="icon-text"> ${cname}</span>
						</a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
							<a class="dropdown-item" href="/logout">Logout</a>
							
						</div>
						</li>
						
						
						
                    </ul>
                    
                </div>
			
            </div>
        </nav>
		
		
		<br>
		<aside id="productinfo">
            <div class="row">
                <div class="container-fluid">
                    <div class="col-xs-12">
                        <div id="product-single">
							<div class="product-single">
                                <div class="row">
                                    <!-- Product Images Carousel -->
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 product-single-image">
                                        <div id="product-slider">
                                            <ul class="slides">
                                                <li>
                                                    <img class="cloud-zoom" src="${movie.getUrl1()}" data-large="${movie.getUrl1()}" alt=""/>
                                                    <a class="fullscreen-button" href="${movie.getUrl1()}">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="product-carousel">
                                            <ul class="slides">
                                                <li>
                                                    <img class="cloud-zoom" src="${movie.getUrl1()}" data-large="${movie.getUrl1()}" alt=""/>
                                                    <a class="fullscreen-button" href="${movie.getUrl1()}">
                                                    </a>
                                                </li>
												
												<li>
                                                    <a class="fancybox" rel="product-images" href="${movie.getUrl2()}"></a>
                                                    <img src="${movie.getUrl2()}" data-large="${movie.getUrl2()}" alt=""/>
                                                </li>
												<li>
                                                    <a class="fancybox" rel="product-images" href="${movie.getUrl2()}"></a>
                                                    <img src="${movie.getUrl2()}" data-large="${movie.getUrl2()}" alt=""/>
                                                </li>
                                                <li>
                                                    <a class="fancybox" rel="product-images" href="${movie.getUrl2()}"></a>
                                                    <img src="${movie.getUrl2()}jpg" data-large="${movie.getUrl2()}" alt="" />
                                                </li>
                                                <li>
                                                    <a class="fancybox" rel="product-images" href="${movie.getUrl2()}"></a>
                                                    <img src="${movie.getUrl2()}" data-large="${movie.getUrl2()}" alt="" />
                                                </li>
                                                
                                            </ul>
                                            <div class="product-arrows">
                                                <div class="left-arrow">
                                                    <i class="icons fa fa-caret-left"></i>
                                                </div>
                                                <div class="right-arrow">
                                                    <i class="icons fa fa-caret-right"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 productinfo-style">
                                        <h1>${movie.getMname()}</h1>
                                        <div class="rating-box">
                                            <div class="rating readonly-rating" data-score="${avgrating}"></div>
                                            <h2>Overall Rating: ${avgrating}</h2>
                                        </div>
                                        <div class="product-price">
                                            <span>(${movie.getYear()})</span>
                                        </div>
                                       <h2> ${movie.getMdescription()}</h2>
									   <br>
                                        <div class="total-price">
                                            <span class="total-price-text">Category: ${movie.getMcategory()}</span>
                                            
                                        </div>
                                        <div class="seller-info">
                                            <h2><span>Rated ${movie.getAgefactor()} </span></h2>
											<br>
                                            
                                        </div>
                                    </div>
                                </div>    
                            </div>
						</div>
					</div>
					
				</div>
			</div>
				
		</aside>
		<aside id="productinfo">	
		<div class="row">
                <div class="container-fluid">
                    <div class="col-xs-12">
						<div id="product-single">
							<div class="product-single">
								
								<form class="form-horizontal" action="/addReview/${movie.mid }">
								   		
								  
								  <div class="form-group">
										<label class="control-label col-sm-2" for="number">Rating:</label>
									
										<div class="col-sm-8">
											<h2>  <select class="mdb-select md-form colorful-select dropdown-primary" name="rating_value">
												  <option value="1"> * </option>
												  <option value="2"> ** </option>
												  <option value="3"> *** </option>
												  <option value="4"> **** </option>
												  <option value="5"> ***** </option>
												</select>	</h2>		
										</div>
										
								  </div>
								  
								  <div class="form-group">
									<label class="control-label col-sm-2" >Review:</label>
									<div class="col-sm-8">
									  <textarea class="form-control" name="review" rows="3"></textarea>
									</div>  
								  </div>
								  
								  <div class="form-group">
									<label class="control-label col-sm-2" > to add-> </label>
									<div class="col-sm-8">
										
									  <button type="submit" class="btn btn-success">Submit</button>
									</div> 
								  </div>
								  
							</div>
						</div>		
					</div>
				</div>			
		</div>	
		</aside>
		 <!-- JavaScript -->
        <script src="/js/modernizr.min.js"></script>
        <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="/js/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/js/jquery.raty.js"></script>
        <!-- Scroll Bar -->
        <script src="/js/perfect-scrollbar.min.js"></script>
        <!-- Cloud Zoom -->
        <script src="/js/zoomsl-3.0.min.js"></script>
        <!-- FancyBox -->
        <script src="/js/jquery.fancybox.pack.js"></script>
        <!-- jQuery REVOLUTION Slider  -->
        <script type="text/javascript" src="/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="/js/jquery.themepunch.revolution.min.js"></script>
        <!-- FlexSlider -->
        <script defer src="/js/flexslider.min.js"></script>
        <!-- IOS Slider -->
        <script src = "/js/jquery.iosslider.min.js"></script>
        <!-- noUi Slider -->
        <script src="/js/jquery.nouislider.min.js"></script>
        <!-- Owl Carousel -->
        <script src="/js/owl.carousel.min.js"></script>
        <!-- Cloud Zoom -->
        <script src="/js/zoomsl-3.0.min.js"></script>
        <!-- SelectJS -->
        <script src="/js/chosen.jquery.min.js" type="text/javascript"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <!-- Main JS -->
        <script src="/js/main-script.js"></script>
        <script src="/js/custom.js"></script>
    </body>
</html>