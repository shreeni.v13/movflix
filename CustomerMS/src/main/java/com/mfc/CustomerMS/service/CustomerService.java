package com.mfc.CustomerMS.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfc.CustomerMS.dao.CustomerRepo;
import com.mfc.CustomerMS.proxy.MovieProxy;
import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;

@Service
public class CustomerService {
	@Autowired
	private CustomerRepo customerRepo;
	
	@Autowired
	private MovieProxy movieProxy;
	
	public Customer register(Customer c)
	{
		return customerRepo.save(c);
	}
	
	public String checkLogin(String email,String password)
	{
		Customer loginCustomer = customerRepo.findById(email).orElse(null);
		if(loginCustomer==null)
			return "Please Register";
		else
		{
			if(loginCustomer.getCemail().equalsIgnoreCase(email)&&loginCustomer.getCpassword().equals(password))
				return "Login Successful";
			else
				return "Wrong Credentials";
		}
	}
	
	public String getUsername(String email)
	{
		return customerRepo.getUsername(email);
	}
	
	public List<Customer> getAll()
	{
		return customerRepo.findAll();
	}
	
	public Customer getCustomer(String email)
	{
		return customerRepo.findById(email).orElse(null);
	}
	public List<Movie> getAllRatings()
	{
		double avg=0;
		int sum=0;
		List<Movie> movies= movieProxy.getAllMovies();
		for(Movie m:movies)
		{
			sum=0;
			avg=0;
			List<Integer> ratings=movieProxy.getAllRatings(m.getMid());
			for(Integer i:ratings) {
				sum=sum+i;
				avg=sum/ratings.size();
			}
			m.setAverage(avg);
//			System.out.println(avg);
		}
		return movies;
	}
}
