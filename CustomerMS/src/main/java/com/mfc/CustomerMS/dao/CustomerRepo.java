package com.mfc.CustomerMS.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mfm.EntityModel.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer,String>{
	@Query("select c.cname from Customer c where c.cemail=?1")
	public String getUsername(String email);
}
