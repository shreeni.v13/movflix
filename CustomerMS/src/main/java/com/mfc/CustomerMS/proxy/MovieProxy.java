package com.mfc.CustomerMS.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;
import com.mfm.EntityModel.Rating;

@FeignClient(name="Movie-MS",url="localhost:8300")
public interface MovieProxy {
	@GetMapping("/getAllMovies")
	public List<Movie> getAllMovies();
	
	@GetMapping("/getAllMoviesByMovieName/{mname}")
	public List<Movie> getAllMoviesByMovieName(@PathVariable("mname") String mname);
	
	@GetMapping("/getAllMoviesByMovieCategory/{mcategory}")
	public List<Movie> getAllMoviesByMovieCategory(@PathVariable("mcategory") String mcategory);
	
	@GetMapping("/getAllMoviesByAgeFactor/{agefactor}")
	public List<Movie> getAllMoviesByAgeFactor(@PathVariable("agefactor") String agefactor);
	
	@GetMapping("/getAllMoviesByYear/{myear}")
	public List<Movie> getAllMovies(@PathVariable("myear") int myear);
	
	@GetMapping("/{mid}")
    public Movie displayMovie(@PathVariable int mid);
	
	@GetMapping("/addRating")
	public void addRating(@RequestParam int mid,@RequestParam String rating_value,@RequestParam String review,@RequestParam String email);
	
	@GetMapping("/getAllRating/{mid}")
	public List<Integer> getAllRatings(@PathVariable("mid") int mid);
	
}
