package com.mfc.CustomerMS.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mfc.CustomerMS.proxy.MovieProxy;
import com.mfc.CustomerMS.service.CustomerService;
import com.mfm.EntityModel.Customer;
import com.mfm.EntityModel.Movie;

@Controller
@Transactional
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private MovieProxy movieProxy;
	
	@Autowired
	private LoadBalancerClient loadBalancerClient;
	
	@PostMapping("/register")
	@ResponseBody
	public Customer register(@RequestBody Customer c)
	{
		return customerService.register(c);
	}
	
	@GetMapping("/login_customer")
	@ResponseBody
	public String login(String email,String password)
	{
		return customerService.checkLogin(email, password);
	}
	
	
	@GetMapping("/movie-homepage/{email}")
	@ResponseBody
	public ModelAndView displayHomePage(@PathVariable("email") String email,HttpServletRequest request) {
		HttpSession session=request.getSession();
		session.setAttribute("email", email);
		String username = customerService.getUsername(email);
		List<Movie> movies=customerService.getAllRatings();
        ModelAndView mv= new ModelAndView();
		mv.addObject("movies", movies);
		mv.addObject("cname", username);
		mv.setViewName("HomePage");
		return mv;
	}
	
	@GetMapping("/search")
	@ResponseBody
	public ModelAndView SearchFunction(@ModelAttribute("type") String type,@ModelAttribute("movie_cat") String movie_cat,HttpServletRequest request)
	{
		ModelAndView mv=new ModelAndView();
		String email=(String)request.getSession(false).getAttribute("email");
		String username = customerService.getUsername(email);
		mv.addObject("cname", username);
		mv.setViewName("Display");
		if(type.equalsIgnoreCase("Movie Name"))
		{
			List<Movie> result = movieProxy.getAllMoviesByMovieName(movie_cat);
			if(result.size()==0) {
				mv.addObject("error", "Sorry No Matches found...");
			}
			else {
				mv.addObject("movies", result);
			}
			
		}
		else if(type.equalsIgnoreCase("Year"))
		{
			int year=Integer.parseInt(movie_cat);
			List<Movie> result = movieProxy.getAllMovies(year);
			if(result.size()==0) {
				mv.addObject("error", "Sorry No Matches found...");
			}
			else {
				mv.addObject("movies", result);
			}
		}
		else if(type.equalsIgnoreCase("Category"))
		{
			List<Movie> result =movieProxy.getAllMoviesByMovieCategory(movie_cat);
			if(result.size()==0) {
				mv.addObject("error", "Sorry No Matches found...");
			}
			else {
				mv.addObject("movies", result);
			}
		}
		else if(type.equalsIgnoreCase("Age Factor"))
		{
			List<Movie> result =movieProxy.getAllMoviesByAgeFactor(movie_cat);
			if(result.size()==0) {
				mv.addObject("error", "Sorry No Matches found...");
			}
			else {
				mv.addObject("movies", result);
			}
		}
		return mv;
	}
	
	
	@GetMapping("/getAllCustomers")
	@ResponseBody
	public List<Customer> getAllCustomers()
	{
		return customerService.getAll();
	}
	
	@GetMapping("/pick/{mid}")
	@ResponseBody
    public ModelAndView displayMovie(@PathVariable int mid){
   
        Movie movie=movieProxy.displayMovie(mid);
        ModelAndView mv= new ModelAndView();
        mv.addObject("movie", movie);
        mv.addObject("cname", "rahul");
        mv.addObject("avgrating", 3);
        mv.setViewName("MovieDetails");
        return mv;
    }
	
	@GetMapping("/addReview/{mid}")
	
	public String addRating(HttpServletRequest request,@PathVariable int mid,@ModelAttribute("rating_value") String rating_value,@ModelAttribute("review") String review)
	{
		String email=(String)request.getSession(false).getAttribute("email");
		Customer c=customerService.getCustomer(email);
		System.out.println(c.getCemail());
		movieProxy.addRating(mid, rating_value, review,email);
		return "redirect:/disp";
	}
	
	
	@GetMapping("/disp")
	@ResponseBody
	public ModelAndView displayHomePage1(HttpServletRequest request) {
		List<Movie> movies=customerService.getAllRatings();
		String email=(String)request.getSession(false).getAttribute("email");
		String username = customerService.getUsername(email);
        ModelAndView mv= new ModelAndView();
		mv.addObject("movies", movies);
		mv.addObject("cname", username);
		mv.setViewName("HomePage");
		return mv;
	}
	
	@GetMapping("/logout")
    public void logout(HttpServletRequest request,HttpServletResponse response) throws IOException
    {
        HttpSession session=request.getSession(false);
        session.invalidate();
//        response.sendRedirect("http://localhost:8200/welcome");
        ServiceInstance instance=loadBalancerClient.choose("Admin-MS");
		 URI uri=URI.create(String.format("http://%s:%s", instance.getHost(),instance.getPort()));
		 System.out.println("URI:"+uri);
		 String url=uri.toString();
		 System.out.println("URL:"+url);
		 try {
			 response.sendRedirect(url+"/welcome");
		 }catch (IOException e1) {
				System.out.println("Error in redirecting");
		 }
    }
}

