package com.mfm.EntityModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Movie {

	@Id
	private int mid;
	private String mname;
	private String mdescription;
	private String mcategory;
	private String url1;
	private String url2;
	private String agefactor;
	private int year;
//	@OneToMany(mappedBy="movie",fetch=FetchType.LAZY)
//	private List<Rating> rating = new ArrayList<Rating>();
	
	@Transient
	private double average;
	
	public Movie() {
		
	}

	public Movie(int mid, String mname, String mdescription, String mcategory, String url1, String url2, String agefactor,
			int year) {
		super();
		this.mid = mid;
		this.mname = mname;
		this.mdescription = mdescription;
		this.mcategory = mcategory;
		this.url1 = url1;
		this.url2 = url2;
		this.agefactor = agefactor;
		this.year= year;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getMdescription() {
		return mdescription;
	}

	public void setMdescription(String mdescription) {
		this.mdescription = mdescription;
	}

	public String getMcategory() {
		return mcategory;
	}

	public void setMcategory(String mcategory) {
		this.mcategory = mcategory;
	}

//	public List<Rating> getRating() {
//		return rating;
//	}
//
//	public void setRating(List<Rating> rating) {
//		this.rating = rating;
//	}
//	

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Movie [mid=" + mid + ", mname=" + mname + ", mdescription=" + mdescription + ", mcategory=" + mcategory
				+ "]";
	}

	public String getUrl1() {
		return url1;
	}

	public void setUrl1(String url1) {
		this.url1 = url1;
	}

	public String getUrl2() {
		return url2;
	}
	

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public void setUrl2(String url2) {
		this.url2 = url2;
	}

	public String getAgefactor() {
		return agefactor;
	}

	public void setAgefactor(String agefactor) {
		this.agefactor = agefactor;
	}

	
}
