package com.mfm.EntityModel;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {

	@Id
	private String aemail;
	private String aname;
	private String apassword;
	private String amobileno;
	private int aflag=-1;
	
	public Admin() {
		
	}

	public Admin(String aemail, String aname, String apassword, String amobileno) {
		super();
		this.aemail = aemail;
		this.aname = aname;
		this.apassword = apassword;
		this.amobileno = amobileno;
		
	}
	
	

	public String getAemail() {
		return aemail;
	}

	public void setAemail(String aemail) {
		this.aemail = aemail;
	}

	public String getAname() {
		return aname;
	}

	public void setAname(String aname) {
		this.aname = aname;
	}

	public String getApassword() {
		return apassword;
	}

	public void setApassword(String apassword) {
		this.apassword = apassword;
	}

	public String getAmobileno() {
		return amobileno;
	}

	public void setAmobileno(String amobileno) {
		this.amobileno = amobileno;
	}

	public int getAflag() {
		return aflag;
	}

	public void setAflag(int aflag) {
		this.aflag = aflag;
	}

	@Override
	public String toString() {
		return "Admin Values [aemail=" + aemail + ", aname=" + aname + ", apassword=" + apassword + ", amobileno=" + amobileno
				+ ", aflag=" + aflag + "]";
	}
	
	
}
