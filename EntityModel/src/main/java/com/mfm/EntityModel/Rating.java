package com.mfm.EntityModel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Rating {

	@Id
	@GeneratedValue
	private int rid;
	private String review;
	private int rating;
	
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Movie movie;
	
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Customer customer;
	
	public Rating() {
	
	}

	public Rating(String review, int rating) {
		super();
		
		this.review = review;
		this.rating = rating;
	}

	
	
	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Rating [rid=" + rid + ", review=" + review + ", rating=" + rating + "]";
	}
	
	
}
