package com.mfm.EntityModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Customer {

	@Id
	private String cemail;
	private String cname;
	private String cpassword;
	private String cmobileno;
	@Temporal(TemporalType.DATE)
//	@DateTimeFormat(pattern="dd-MM-yyyy")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date cdob;
	
	@OneToMany(mappedBy="customer")
	private List<Rating> rating= new ArrayList<Rating>();
	
	public Customer() {
		
	}

	public Customer(String cemail, String cname, String cpassword, String cmobileno, Date cdob) {
		super();
		this.cemail = cemail;
		this.cname = cname;
		this.cpassword = cpassword;
		this.cmobileno = cmobileno;
		this.cdob = cdob;
	}

	
	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getCpassword() {
		return cpassword;
	}

	public void setCpassword(String cpassword) {
		this.cpassword = cpassword;
	}

	public String getCmobileno() {
		return cmobileno;
	}

	public void setCmobileno(String cmobileno) {
		this.cmobileno = cmobileno;
	}

	public Date getCdob() {
		return cdob;
	}

	public void setCdob(Date cdob) {
		this.cdob = cdob;
	}

	public List<Rating> getRating() {
		return rating;
	}

	public void setRating(List<Rating> rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "Customer [cemail=" + cemail + ", cname=" + cname + ", cpassword=" + cpassword + ", cmobileno="
				+ cmobileno + ", cdob=" + cdob + "]";
	}
	
	
}
